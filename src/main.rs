extern crate enet;

use enet::*;

fn main() {
  let server = Enet::new().expect("enet_initialize failed");
  let address = Address::new(std::net::Ipv4Addr::LOCALHOST, 17091);

  println!("Starting ENet Server.");
  
  let mut host = server.create_host::<()>(
    Some(&address),
    32,
    ChannelLimit::Maximum,
    BandwidthLimit::Unlimited,
  BandwidthLimit::Unlimited).expect("could not create host");

  println!("ENet Server started.");

  loop {
    match host.service(1000).expect("event checking failed") {
      Some(Event::Connect(_)) => println!("A client connecte."),
      _ => ()
    }
  }
}
